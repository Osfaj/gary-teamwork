Day 4 For Practice Purpose - Binar x Glints

Section 1 : HTML Basic Exercises
1. Buat halaman website yang menampilkan Nama anda pada layar
2. Buat halaman website yang menampilkan nomor 1-10 pada layar
3. Buat halaman website yang menampilkan Title "This is a webpage"
4. Buat Halaman yang menampilkan pesan "Kapan halaman ini dibuat? Jawaban lihat pada tab judul halaman ini." pada layar, dan tentukan tab title halaman dengan tanggal saat ini
5. Buat halaman webpage yang menampilkan text apapun pada layar tanpa menambahkan "head section".

Section 2 : HTML text exercises
1. Tampilkan namamu pada layar dengan warna selain hitam
2. Tampilkan Numbers 1-10, dengan tiap number diberikan warna yang berbeda
3. Tampilkan Namamu dengan Font Tahoma
4. Tampilkan paragraph dengan 4-5 kalimat dengan tiap kalimat harus berbeda jenis Font
5. Tampilkan paragraph yang mendeskripsikan sebuah buku, termasuk Judul dan penulisnya. Nama dan Judul harus bergaris bawah. Kata kerja italic dan bold

Section 3: Text formatting Exercises
1. Tampilkan 10 merk mobil dengan ditambahkan "line break" pada tiap merk mobil.
2. Tampilkan 2 paragraf yang keduanya menggunakan perintah &nbsp pada awal paragraf
3. Tampilkan Judul header h1 (Apapun) lalu tambahkan garis horizontal yang memenuhi lebar layar 100%. Lalu dibawah garis horizontal tampilkan paragraf yang relevan dengan judul h1 tadi
4. Tampilkan list definisi 5 items (HTML, Git, Internet, Smartphone, CSS)
5. Tampilkan 2 alamat dengan format yang sama pada halaman depan sebuah amplop (untuk pengirim terletak pada pojok kiri amplop, untuk penerima terletak pada tengah amplop)

Section 4 : HTML Link Exercises
1. Buatlah Link beberapa Search Engine (Google, Yahoo, duckduckgo)
2. Buatlah Links pada 5 halaman yang berbeda dari 5 websites yang harus terbuka di open tab ketika di klik.
3. Buatlah halaman dengan link/tombol yang mengarahkan dasar dari halaman (jadi konten harus cukup banyak)
4. Buatlah kebalikan dari exercise nomor 3
5. Gabungan Keduanya dari nomor 3 dan 4 jadi satu 
=======
